const mongoose = require("mongoose");

const favorisSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
      require,
    },
    postId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "posts",
      require,
    },
    type: {
      type: String,
      require,
    },
  },
  { timestamps: true }
);
const FavorisrModel = mongoose.model("Favoris", favorisSchema);
module.exports = FavorisrModel;
