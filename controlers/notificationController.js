const NotifService = require("../services/NotifService");

exports.getNotification = (req, res) => {
  let data = req.body;

  return NotifService.getNotification(data, res);
};
exports.setNotification = (req, res) => {
  let data = req.body;

  return NotifService.setNotification(data, res);
};
