const express = require("express");

const postsController = require("../controlers/postsController");
const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // __dirname valeur de path actuel
    cb(null, path.join(__dirname, "../uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname.replace(/\s/g, ""));
  },
  language: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.language.replace(/\s/g, ""));
  },
});

var upload = multer({
  storage: storage,
  //
});

const router = express.Router();

router.post("/setpost", upload.any(), postsController.setPost);
router.get("/posts/:id", postsController.Posts);
router.post("/remove-posts", postsController.removePosts);

router.get("/getPosts/:id", postsController.getPosts);
router.get("/getPostsClient/:id", postsController.getPostsClient);
router.get("/getPost-detail/:id", postsController.getPostDetails);
router.post("/postValidation/:id", postsController.postValidation);

module.exports = router;
