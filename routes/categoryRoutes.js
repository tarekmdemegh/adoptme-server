const express = require("express");

const CategoryController = require("../controlers/categoryController");

const router = express.Router();

router.post("/setCategory", CategoryController.setCategory);

module.exports = router;
