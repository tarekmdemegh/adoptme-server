const favoriesServecies = require("../services/favoriesServecies");

exports.setFavories = (req, res) => {
  let data = req.body;
  return favoriesServecies.setFavories(data, res);
};

exports.getFavories = (req, res) => {
  let data = req.body;
  return favoriesServecies.getFavories(data, res);
};
