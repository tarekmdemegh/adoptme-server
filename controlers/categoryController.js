const categoryservice = require("../services/categoryservice");

exports.setCategory = (req, res) => {
  let data = req.body;

  return categoryservice.setCategory(data, res);
};
