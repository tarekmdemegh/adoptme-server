const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const userSchema = mongoose.Schema({
  FullName: {
    type: String,
    require,
  },
  email: {
    type: String,
    unique: true,
    require,
  },
  age: {
    type: String,
  },
  adress: {
    type: String,
  },
  mobile: {
    type: String,
  },
  facebook: {
    type: String,
  },
  whatsapp: {
    type: String,
  },
  password: {
    type: String,
    require,
  },
});

userSchema.pre("save", function save(next) {
  const user = this;
  if (!user.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function (condidatePassword) {
  const user = this;
  return new Promise((resolve, reject) => {
    bcrypt.compare(condidatePassword, user.password, (err, isMatch) => {
      if (err) {
        return reject(err);
      }
      if (!isMatch) {
        return reject(false);
      }

      resolve(true);
    });
  });
};

const UserModel = mongoose.model("User", userSchema);
module.exports = UserModel;
