const mongoose = require("mongoose");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const NotifModel = require("../models/Notification");

/**
 * route('/singup')
 * params:data
 */
exports.setNotification = async (data, res) => {
  // check req body
  try {
    // init
    const { userId, postId, postUserId } = data;
    console.log("## !!!!!!!!!!!!!!!!!!!!!!!!! ", userId, postId);
    const newNotifToAdd = await new NotifModel({
      userId: mongoose.Types.ObjectId(userId),
      postId: mongoose.Types.ObjectId(postId),
      postUserId: mongoose.Types.ObjectId(postUserId),
    });
    console.log("newNotifToAdd", newNotifToAdd);
    // check this email is unique
    const NotifsExist = await NotifModel.findOne({
      userId: mongoose.Types.ObjectId(userId),
      postId: mongoose.Types.ObjectId(postId),
    });
    console.log("NotifsExist", NotifsExist);
    if (NotifsExist) {
      res.status(400).send({ Status: "fail", message: "Favories  is exist" });
    } else {
      // store user

      newNotifToAdd.save((err) => {
        if (err) {
          res.send(err);
        }
        console.log("success");
        res.send({ status: "success", data: newNotifToAdd, action: true });
      });
    }
  } catch (err) {
    res.send(err);
  }
};

exports.getNotification = async (data, res) => {
  // check req body

  try {
    // init
    const { userId, postId, postUserId } = data;

    const notifs = await NotifModel.aggregate([
      { $match: { postUserId: mongoose.Types.ObjectId(postUserId) } },
      {
        $lookup: {
          from: "posts",
          localField: "postId",
          foreignField: "_id",
          as: "post",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",

          as: "user",
        },
      },
      {
        $group: {
          _id: "$_id", // id notifs
          idpost: { $first: "$postId" },
          userID: { $first: "$userId" },
          postUserId: { $first: "$postUserId" },
          post: { $first: "$post" },
          user: { $first: "$user" },
        },
      },
    ]);

    if (!notifs) {
      res.send({ Status: "fail", message: "No notifs", action: false });
    } else {
      // store user
      console.log("success");
      res.send({ status: "success", data: notifs, action: true });
    }
  } catch (err) {
    res.send(err);
  }
};
