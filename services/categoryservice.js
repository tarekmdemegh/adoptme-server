const mongoose = require("mongoose");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const CategoryModel = require("../models/category");

/**
 * route('/singup')
 * params:data
 */
exports.setCategory = async (data, res) => {
  // check req body
  try {
    // init
    const { name } = data;
    const category = await new CategoryModel({ name });
    // check this email is unique
    const categoryExist = await CategoryModel.findOne({ name });
    if (categoryExist) {
      res.status(400).send({ Status: "fail", message: "category exist" });
    } else {
      // store user

      category.save((err) => {
        if (err) {
          res.send(err);
        }

        res.send({ status: "success", data: category });
      });
    }
  } catch (err) {
    res.send(err);
  }
};
