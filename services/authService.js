const mongoose = require("mongoose");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const UserModel = require("../models/user");
const AdminModel = require("../models/admin");

exports.signupAdmin = async (data, res) => {
  // check req body
  console.log("user try to signupAdmin");

  try {
    // init
    const { name, lastname, email, password } = data;

    const Admin = await new AdminModel({
      name,
      lastname,
      email,
      password,
    });
    // check this email is unique
    const adminExist = await AdminModel.findOne({ email });

    if (adminExist) {
      return res.send({ Status: "fail", message: "admin exist" });
    } else {
      // store user

      Admin.save((err) => {
        if (err) {
          res.send(err);
        }
        const token = jwt.sign({ userId: Admin._id }, "MYSECRETKEY");

        return res.send({ status: "success", data: Admin, token });
      });
    }
  } catch (err) {
    return res.send(err);
  }
};
exports.signinAdmin = async (data, res) => {
  // check req body

  try {
    // init
    const { email, password } = data.body;

    const c = await AdminModel.countDocuments();

    if (!email && !password) {
      return res.send({
        status: "fail",
        message: "must Provid email and password",
        count: c,
      });
    }
    const admin = await AdminModel.findOne({ email });

    if (!admin) {
      console.log("email not found or email not invalid");

      return res.send({
        status: "fail",
        message: "email not found or email invalid",
      });
    }
    try {
      await admin.comparePassword(password);
      const token = jwt.sign({ UserId: admin._id }, "MYSECRETKEY");
      console.log("admin connected successfully");

      return res.send({
        status: "success",
        data: admin,
        token,
        message: "admin connected successfully",
      });
    } catch (err) {
      return res.send({ status: "fail", message: "password Invalid " });
    }
  } catch (err) {
    res.send(err);
  }
};

//
exports.updatetUserInfo = async (data, res) => {
  //
  const {
    FullName,
    email,
    password,
    age,
    adress,
    mobile,
    facebook,
    whatsapp,
  } = data;
  let UpdateData = {
    FullName,
    email,
    password,
    age,
    adress,
    mobile,
    facebook,
    whatsapp,
  };

  try {
    await UserModel.findOneAndUpdate({ _id: data.userId }, data, function (
      err
    ) {
      if (err) {
        res.json({
          status: "error",
          message: err,
        });
      } else {
        res.json({
          status: "success",
          message: "user successfully updated",
        });
      }
    });

    await UserModel.findOneAndUpdate({ _id: userId }, UpdateData)
      .sort({ createdAt: -1 })
      .exec(async function (error, data) {
        if (error) {
          res.json({
            status: "error",
            message: error,
          });
        } else {
          res.status(200).json({
            status: "success",
            data: data,
          });
        }
      });
  } catch {
    return res.json({
      status: "error",
      message: "Erreur de serveur",
    });
  }
};
//
exports.getUserInfo = async (data, res) => {
  //
  const { userId } = data;

  try {
    await UserModel.findOne({ _id: userId })
      .sort({ createdAt: -1 })
      .exec(async function (error, data) {
        if (error) {
          res.json({
            status: "error",
            message: error,
          });
        } else {
          res.json({
            status: "success",
            data: data,
          });
        }
      });
  } catch {
    return res.json({
      status: "error",
      message: "Erreur de serveur",
    });
  }
};
//

exports.signup = async (data, res) => {
  // check req body
  console.log("user try to signup");

  try {
    // init
    const {
      FullName,
      email,
      password,
      age,
      adress,
      phone,
      facebook,
      whatsapp,
    } = data;

    const user = await new UserModel({
      FullName,
      email,
      age,
      adress,
      mobile: phone,
      facebook,
      whatsapp,
      password,
    });
    // check this email is unique
    const userExist = await UserModel.findOne({ email });

    if (userExist) {
      return res.send({ Status: "fail", message: "user exist" });
    } else {
      // store user

      user.save((err) => {
        if (err) {
          res.send(err);
        }
        const token = jwt.sign({ userId: user._id }, "MYSECRETKEY");

        return res.send({ status: "success", data: user, token });
      });
    }
  } catch (err) {
    return res.send(err);
  }
};

exports.signin = async (data, res) => {
  // check req body
  try {
    // init
    const { email, password } = data;

    if (!email && !password) {
      return res.send({
        status: "fail",
        message: "must Provid email and password",
      });
    }
    const user = await UserModel.findOne({ email });

    if (!user) {
      console.log("email not found or email not invalid");

      return res.send({
        status: "fail",
        message: "email not found or email invalid",
      });
    }
    try {
      await user.comparePassword(password);
      const token = jwt.sign({ UserId: user._id }, "MYSECRETKEY");
      console.log("user connected successfully");

      return res.send({
        status: "success",
        data: user,
        token,
        message: "user connected successfully",
      });
    } catch (err) {
      return res.send({ status: "fail", message: "password Invalid " });
    }
  } catch (err) {
    res.send(err);
  }
};
exports.getUserList = async (data, res) => {
  try {
    var decoded = jwt.verify(data.headers["x-access-token"], "MYSECRETKEY");

    const admin = await AdminModel.findOne({ _id: decoded.UserId });

    if (admin) {
      const users = await UserModel.find();
      return res.send({
        status: "succes",
        data: users,
      });
    } else {
      return res.send({
        status: "fail",
        message: "access not valid ",
      });
    }
  } catch (e) {
    return res.send({
      status: "fail",
      message: "server error",
    });
  }
};
exports.removeUser = async (req, res) => {
  var id = req.body.id;

  try {
    var decoded = jwt.verify(req.headers["x-access-token"], "MYSECRETKEY");

    const admin = await AdminModel.findOne({ _id: decoded.UserId });

    if (admin) {
      if (id) {
        await UserModel.findOneAndDelete({ _id: id }).exec(async function (
          error,
          data
        ) {
          if (error) {
            res.json({
              status: "error",
              message: error,
            });
          } else {
            res.json({
              status: "success",
              data: data,
            });
          }
        });
      }
    } else {
      return res.send({
        status: "fail",
        message: "access not valid ",
      });
    }
  } catch (e) {
    return res.send({
      status: "fail",
      message: "server error",
    });
  }
};
