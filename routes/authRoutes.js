const express = require("express");

const AuthController = require("../controlers/authController");

const router = express.Router();

router.post("/signup", AuthController.signup);
router.post("/signin", AuthController.signin);
router.post("/getUserInfo", AuthController.getUserInfo);
router.post("/updatetUserInfo", AuthController.updatetUserInfo);
//
router.post("/signupAdmin", AuthController.signupAdmin);
router.post("/signinAdmin", AuthController.signinAdmin);
router.post("/getUserList", AuthController.getUserList);
router.post("/remove-User", AuthController.removeUser);

module.exports = router;
