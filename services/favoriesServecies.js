const mongoose = require("mongoose");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const FavoriesModel = require("../models/favoris");

/**
 * route('/singup')
 * params:data
 */
exports.setFavories = async (data, res) => {
  // check req body
  try {
    // init
    const { userId, postId, type } = data;
    console.log("## !!!!!!!!!!!!!!!!!!!!!!!!! ", userId, postId, type);
    const newFavoriesToAdd = await new FavoriesModel({
      userId: mongoose.Types.ObjectId(userId),
      postId: mongoose.Types.ObjectId(postId),
      type: type,
    });
    console.log("newFavoriesToAdd", newFavoriesToAdd);
    // check this email is unique
    const FavoriesExist = await FavoriesModel.findOne({
      userId: mongoose.Types.ObjectId(userId),
      postId: mongoose.Types.ObjectId(postId),
      type: type,
    });
    console.log("FavoriesExist", FavoriesExist);
    if (FavoriesExist) {
      FavoriesModel.findOneAndRemove(
        {
          userId: mongoose.Types.ObjectId(userId),
          postId: mongoose.Types.ObjectId(postId),
          type: type,
        },
        function (err, offer) {
          if (err) {
            res.json({
              status: "error",
              message: err,
            });
          } else {
            console.log("deleted !");
          }
        }
      );

      res.status(400).send({ Status: "fail", message: "Favories  is exist" });
    } else {
      // store user

      newFavoriesToAdd.save((err) => {
        if (err) {
          res.send(err);
        }
        console.log("success");
        res.send({ status: "success", data: newFavoriesToAdd, action: true });
      });
    }
  } catch (err) {
    res.send(err);
  }
};

exports.getFavories = async (data, res) => {
  // check req body
  try {
    // init
    const { userId, postId, type } = data;

    const Favories = await FavoriesModel.aggregate([
      { $match: { userId: mongoose.Types.ObjectId(userId) } },
      {
        $lookup: {
          from: "posts",
          localField: "postId",
          foreignField: "_id",
          as: "post",
        },
      },
      {
        $group: {
          _id: "$_id", // id favorie
          idpost: { $first: "$postId" },
          post: { $first: "$post" },
        },
      },
    ]);

    if (!Favories) {
      res
        .status(400)
        .send({ Status: "fail", message: "No Favories", action: false });
    } else {
      // store user
      console.log("success", Favories);
      res.send({ status: "success", data: Favories, action: true });
    }
  } catch (err) {
    res.send(err);
  }
};
