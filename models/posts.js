const mongoose = require("mongoose");

const postsSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
      require,
    },
    // category: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: "categories",
    //   require,
    // },
    type: Number,
    status: {
      type: Boolean,
    },
    images: [
      {
        name: {
          type: String,
          trim: true,
          required: false,
        },
      },
    ],

    animalsDetails: String,
    Events: String,
    shopDetails: String,
    veterinaryDetails: String,
  },
  { timestamps: true }
);
const PostsModel = mongoose.model("Post", postsSchema);
module.exports = PostsModel;

// [
//     {
//       _id: false,
//       name: { type: String },
//       desc: { type: String },
//       age: { type: Number },
//       gender: { type: String },
//       poids: { type: String },
//       color: { type: String },
//     },
//   ],
