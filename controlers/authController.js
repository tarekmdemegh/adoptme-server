const authService = require("../services/authService");
exports.removeUser = (req, res) => {
  return authService.removeUser(req, res);
};

exports.getUserList = (req, res) => {
  return authService.getUserList(req, res);
};

exports.signupAdmin = (req, res) => {
  let data = req.body;
  console.log("admin try to get list login");
  return authService.signupAdmin(data, res);
};
exports.signinAdmin = (req, res) => {
  let data = req.body;
  console.log("admin try to sign in");
  return authService.signinAdmin(req, res);
};
exports.signup = (req, res) => {
  let data = req.body;
  console.log("user try to sign up");
  return authService.signup(data, res);
};

exports.signin = (req, res) => {
  let data = req.body;
  console.log("user try to sign in");

  return authService.signin(data, res);
};

exports.getUserInfo = (req, res) => {
  let data = req.body;
  console.log("user try to getUserInfo");
  return authService.getUserInfo(req.body, res);
};
exports.updatetUserInfo = (req, res) => {
  let data = req.body;
  console.log("user try to updatetUserInfo");

  return authService.updatetUserInfo(req.body, res);
};
