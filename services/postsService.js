const mongoose = require("mongoose");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const CategoryModel = require("../models/category");
const PostModel = require("../models/posts");
const UserModel = require("../models/user");

exports.postValidation = async (req, res) => {
  // check req body
  var id = req.params.id;
  let status = req.body.status;

  try {
    await PostModel.findOneAndUpdate({ _id: id }, { status: status }).exec(
      async function (error, data) {
        if (error) {
          res.json({
            status: "error",
            message: error,
          });
        } else {
          res.status(200).json({
            status: "success",
            message: "post validation ",
            action: status,
          });
        }
      }
    );
  } catch {
    return res.json({
      status: "error",
      message: "Erreur de serveur",
    });
  }
};

exports.setPost = async (data, res, files) => {
  // check req body

  try {
    // init
    const {
      idUser,
      type,
      animalsDetails,
      Events,
      shopDetails,
      veterinaryDetails,
    } = data.body;

    if (!type && !idUser) {
      return res.send({
        status: "fail",
        message: "idUser and type required !",
      });
    } else {
      let arr = [];
      files.forEach((element) => {
        arr.push({ name: `/uploads/${element.filename}` });
      });

      const post = await new PostModel({
        userId: mongoose.Types.ObjectId(idUser),
        type,
        images: arr,
        animalsDetails,
        Events,
        shopDetails,
        veterinaryDetails,
        status: false,
      });
      post.save((err) => {
        if (err) {
          res.send(err);
        }
        res.send({ status: "success", data: post });
      });
    }
  } catch (err) {
    res.send(err);
  }
};
//
exports.getPostsClient = async (req, res) => {
  // check req body
  var id = req.params.id;

  try {
    await PostModel.find({ type: id, status: true })
      .sort({ createdAt: -1 })
      .exec(async function (error, data) {
        if (error) {
          res.json({
            status: "error",
            message: error,
          });
        } else {
          res.status(200).json({
            status: "success",
            data: data,
          });
        }
      });
  } catch {
    return res.json({
      status: "error",
      message: "Erreur de serveur",
    });
  }
};
//
exports.getPosts = async (req, res) => {
  // check req body
  var id = req.params.id;

  try {
    await PostModel.find({ type: id })
      .sort({ createdAt: -1 })
      .exec(async function (error, data) {
        if (error) {
          res.json({
            status: "error",
            message: error,
          });
        } else {
          res.status(200).json({
            status: "success",
            data: data,
          });
        }
      });
  } catch {
    return res.json({
      status: "error",
      message: "Erreur de serveur",
    });
  }
};

exports.Posts = async (req, res) => {
  // check req body
  var id = req.params.id;

  try {
    await PostModel.find({ userId: id })
      .sort({ createdAt: -1 })
      .exec(async function (error, data) {
        if (error) {
          res.json({
            status: "error",
            message: error,
          });
        } else {
          res.status(200).json({
            status: "success",
            data: data,
          });
        }
      });
  } catch {
    return res.json({
      status: "error",
      message: "Erreur de serveur",
    });
  }
};

exports.getPostDetails = async (req, res) => {
  // check req body
  var id = req.params.id;

  try {
    await PostModel.findOne({ _id: id })
      .sort({ createdAt: -1 })
      .exec(async function (error, data) {
        if (error) {
          res.json({
            status: "error",
            message: error,
          });
        } else {
          res.status(200).json({
            status: "success",
            data: data,
          });
        }
      });
  } catch {
    return res.json({
      status: "error",
      message: "Erreur de serveur",
    });
  }
};

exports.removePosts = async (req, res) => {
  var id = req.body.id;
  var idPost = req.body.idPost;

  try {
    var decoded = jwt.verify(req.headers["x-access-token"], "MYSECRETKEY");

    const userExist = await UserModel.findOne({ _id: decoded.UserId });

    if (userExist) {
      if (idPost) {
        const post = await PostModel.findOne({ _id: idPost, userId: id });

        if (post) {
          await PostModel.findOneAndDelete({ _id: idPost, userId: id }).exec(
            async function (error, data) {
              if (error) {
                console.log("error");
                res.json({
                  status: "error",
                  message: error,
                  action: false,
                });
              } else {
                console.log("success");

                res.json({
                  status: "success post deleted !",
                  action: true,
                });
              }
            }
          );
        } else {
          res.json({
            status: "post not found",
            action: false,
          });
        }
      }
    } else {
      return res.send({
        status: "fail",
        message: "access not valid ",
      });
    }
  } catch (e) {
    return res.send({
      status: "fail",
      message: "server error access not valid ",
    });
  }
};
