const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const adminSchema = mongoose.Schema({
  name: {
    type: String,
    require,
  },
  email: {
    type: String,
    unique: true,
    require,
  },
  lastname: {
    type: String,
  },

  password: {
    type: String,
    require,
  },
});

adminSchema.pre("save", function save(next) {
  const admin = this;
  if (!admin.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(admin.password, salt, (err, hash) => {
      if (err) {
        return next(err);
      }
      admin.password = hash;
      next();
    });
  });
});

adminSchema.methods.comparePassword = function (condidatePassword) {
  const admin = this;
  return new Promise((resolve, reject) => {
    bcrypt.compare(condidatePassword, admin.password, (err, isMatch) => {
      if (err) {
        return reject(err);
      }
      if (!isMatch) {
        return reject(false);
      }

      resolve(true);
    });
  });
};

const AdminModel = mongoose.model("Admin", adminSchema);
module.exports = AdminModel;
