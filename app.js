const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const authRoutes = require("./routes/authRoutes");
const categoryRoutes = require("./routes/categoryRoutes");
const postsRoutes = require("./routes/postsRoutes");
const favoriesRoutes = require("./routes/favoriesRoutes");
const notification = require("./routes/notification");

const imagemin = require("imagemin");
const imageminJpegtran = require("imagemin-jpegtran");
const imageminPngquant = require("imagemin-pngquant");
const port = 3000;

// url de base donneé
const urldb = "mongodb://localhost:27017/adoptme";
mongoose.connect(urldb);

const db = mongoose.connection;

mongoose.connection.on("connected", () => {
  console.log("connected to mongo instance !");
});
mongoose.connection.on("error", (err) => {
  console.log("error to connec with mongosse instanc ", err);
});
// declaration de biblotheque dans un variable "app"
const app = express();

app.use(bodyParser.json());
// routes  auth
app.use(authRoutes);
app.use(categoryRoutes);
app.use(postsRoutes);
app.use(favoriesRoutes);
app.use(notification);

app.use("/uploads", express.static(__dirname + "/uploads"));
(async () => {
  const files = await imagemin(["uploads/*.{jpg,png}"], {
    destination: "uploads",
    plugins: [
      imageminJpegtran(),
      imageminPngquant({
        quality: [0.1, 0.1],
      }),
    ],
  });

  //=> [{data: <Buffer 89 50 4e …>, destinationPath: 'build/images/foo.jpg'}, …]
})();
app.listen(port, function () {
  console.log("hello server ! " + port);
});
