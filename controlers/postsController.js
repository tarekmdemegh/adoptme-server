const postsService = require("../services/postsService");

exports.setPost = (req, res) => {
  let data = req;

  return postsService.setPost(data, res, req.files);
};

exports.getPosts = (req, res) => {
  return postsService.getPosts(req, res);
};
exports.Posts = (req, res) => {
  return postsService.Posts(req, res);
};
exports.getPostDetails = (req, res) => {
  return postsService.getPostDetails(req, res);
};

exports.getPostsClient = (req, res) => {
  return postsService.getPostsClient(req, res);
};

exports.postValidation = (req, res) => {
  return postsService.postValidation(req, res);
};
exports.removePosts = (req, res) => {
  return postsService.removePosts(req, res);
};
