const mongoose = require("mongoose");

const NotifSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
      require,
    },
    postId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "posts",
      require,
    },
    postUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
      require,
    },
  },
  { timestamps: true }
);
const NotifModel = mongoose.model("Notif", NotifSchema);
module.exports = NotifModel;
