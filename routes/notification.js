const express = require("express");

const NotificationController = require("../controlers/notificationController");

const router = express.Router();

router.post("/setNotification", NotificationController.setNotification);
router.post("/getNotification", NotificationController.getNotification);

module.exports = router;
