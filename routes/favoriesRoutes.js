const express = require("express");

const FavoriesController = require("../controlers/FavoriesController");

const router = express.Router();

router.post("/setFavories", FavoriesController.setFavories);
router.post("/getFavories", FavoriesController.getFavories);

module.exports = router;
